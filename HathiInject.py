#! /usr/bin/env python3

"""
Created my Michael Dill - 2017

This program has been designed to systematically  inject header/tag metadata
into individual TIFF files, using a pre-created CSV file as a data source.

All arguments and options can be found using the man page, or calling the
'--help' or `-h` flags.

Before running the makefile, it is important that you modify the two options
just below this comment section...

"""

######################################################################
#
# Edit this info
#
######################################################################

# Author data
CONST_CURRENT_AUTHOR = "Virginia Tech - University Libraries - CPT"

# Your ZULU time differential when not on Daylight Savings Time
# EG: EST is -05:00, so you would use "-05"
CONST_NON_DST_OFFSET = "-05"

######################################################################
#
# Do not edit past this point
#
######################################################################

import argparse  # We will parse flags/arguments for the user
import os  # To get directory information
import csv  # To convert CSV data into a workable array
import datetime  # HathiTrust requires ridiculous date/time formatting
from PIL import Image, TiffImagePlugin  # I hate this part

# Create arguments for the user
parser = argparse.ArgumentParser( prog = 'HathiInject' )

#parser.add_argument( "-n", "--number", type = int,
#        help = "Specify the number of images" )
parser.add_argument( "-D", "--dst", type = int, choices = [0, 1],
        help = "set Daylight Savings time: 0=False, 1=True" )
parser.add_argument( "-t", "--time", type = str,
        help = "manually set the scanned time for the images" )
parser.add_argument( "-o", "--output", type = str,
        help = "specify the output directory, to save the modified TIFF files" )
parser.add_argument( "-v", "--verbose", action = "store_true",
        help = "initialize verbose output" )
parser.add_argument( "--version", action="version", version="%(prog)s 1.6" )
requiredNamed = parser.add_argument_group( "required arguments" )
requiredNamed.add_argument( "-d", "--directory", type = str,
        help = "specify the directory where the images are stored",
        required = True )
requiredNamed.add_argument( "-f", "--filename", type = str,
        help = "specify the CSV filename", required = True )

args = parser.parse_args()

######################################################################
#
# Define our auxiliary functions
#
######################################################################

# Define a function to hammer out the correct date/time format
def getTime():
    CONST_DST_OFFSET = '%03d' % ( int( CONST_NON_DST_OFFSET ) + 1 )

    now = datetime.datetime.now()

    # If it's currently DST
    if args.dst or ( now.month < 3 ) or ( now.month > 11 ):
        differential = CONST_DST_OFFSET
    # If it's not currently DST
    else:
        differential = CONST_NON_DST_OFFSET
            
    # "DateTime in ISO8601 format for image capture"
    if args.time:
        if args.dst and len( args.time ) == 19:
            ourTime = args.time + differential + ":00"
        elif ( args.dst == None or args.dst == 0 ) and len( args.time ) == 25:
            ourTime = args.time
        else:
            print( "You have entered bad time/date info, try again" )
            quit()
    else:
        ourTime =  now.strftime( "%Y-%m-%dT%H:%M:%S" ) + differential + ":00"

    return [ourTime]

# Define a function to display our new header data in verbose mode
def displayTIFF( im ):
    print( "Document Name: %r" % im.tag[269] )
    print( "Artist: %r" % im.tag[315] )
    print( "Make: %r" % im.tag[271] )
    print( "Model: %r" % im.tag[272] )
    print( "Software: %r" % im.tag[305] )
    print( "DateTime: %r\n" % im.tag[306] )

    return

######################################################################
#
# Begin our program
#
######################################################################

# Grab our CSV data as a list/array
with open( args.filename, 'r' ) as f:
    reader = csv.reader( f )
    ourList = list( reader )

# Skip the first (0) row of our CSV file, as it is worthless column labels
rowCount = 1
# Get the current day/time, given our arguments
today = getTime()
# Set our TIFF options to allow us to write data
TiffImagePlugin.WRITE_LIBTIFF = False

# Open each file in the given directory
for fileName in os.listdir( args.directory ):

    # Skip nebulous system files, because everything other than Linux is terrible
    if fileName != ".DS_Store" and fileName != "Thumbs.db":
        im = Image.open( args.directory + fileName )

        info = TiffImagePlugin.ImageFileDirectory()

        # "<barcode>/<image filename>"
        info[269] = ourList[rowCount][0] + '/' + fileName
        # "Scanner manufacturer"
        info[271] = ourList[rowCount][1]
        # "Scanner model number"
        info[272] = ourList[rowCount][2] 
        # "Scanner software version number"
        info[305] = ourList[rowCount][3]
        # "Date and time"
        info[306] = today
        # "Person scanning's name"
        info[315] = CONST_CURRENT_AUTHOR

        # Display our header data if in verbose mode
        if args.verbose:
            # displayTIFF( im )
            print( info )

        # Change our save directory, if specified
        if args.output:
            path = args.output
        else:
            path = args.directory
        
        Image.DEBUG = True
        im.save( path + fileName, tiffinfo = info )

        rowCount += 1

# Reset our write settings, now that we're done with all of our files
#TiffImagePlugin.WRITE_LIBTIFF = False

